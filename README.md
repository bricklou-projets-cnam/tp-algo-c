# pacman sdl

## Prérequis

Pour pouvoir compiler le projet, les outils suivants sont nécessaires :
- Clang
- [XMake](https://xmake.io/)

## Compilation

### Linux

```sh
xmake f -p linux -a x64 -m release --toolchain=clang
xmake build
```

### Windows

```sh
xmake f -p windows -a x64 -m release --toolchain=clang
xmake build
```

## Exécution

```sh
xmake run
```