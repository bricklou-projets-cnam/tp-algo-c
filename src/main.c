#include "game/game.h"
#include <stdlib.h>
#include <stdio.h>

int main() {

    if (!game_create()) {
        printf("Error creating game\n");
        return 1;
    }

    game_update();

    game_destroy();

    return 0;
}