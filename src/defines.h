#pragma once

#include "platform.h"

// Unsigned int types.
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;

// Signed int types.
typedef signed char i8;
typedef signed short i16;
typedef signed int i32;
typedef signed long long i64;

// Floating point types
typedef float f32;
typedef double f64;

// Boolean types
typedef int b32;
typedef char b8;

#define true 1
#define false 0

#define SCALE_FACTOR 3
#define SCREEN_WIDTH 208
#define SCREEN_HEIGHT 256

#define WINDOW_WIDTH SCREEN_WIDTH*SCALE_FACTOR
#define WINDOW_HEIGHT SCREEN_HEIGHT*SCALE_FACTOR


#define CELL_SIZE 8
#define FRAME_RATE 60

// Audio values
#define MIX_DEFAULT_FREQUENCY 44100
#define MIX_DEFAULT_FORMAT MIX_DEFAULT_FORMAT
#define MIX_DEFAULT_CHANNELS 2
#define MIX_DEFAULT_CHUNKSIZE 2048