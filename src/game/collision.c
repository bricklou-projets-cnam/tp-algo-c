#include "collision.h"
#include "SDL_rect.h"
#include "application/application.h"
#include "application/math.h"
#include "defines.h"
#include "game/collision.h"
#include "game/entities/ghost.h"
#include "views/menu.h"
#include <SDL2/SDL_mixer.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

static collision_data_t *collision_data;

void collision_init() {
  collision_data = malloc(sizeof(collision_data_t));
  collision_data->dots_count = 0;

  for (int i = 0; i < GHOST_COLOR_COUNT; i++) {
    collision_data->ghost_position[i] = (vec2){0, 0};
  }

  for (int y = 0; y < COLLISION_MAP_HEIGHT; y++) {
    for (int x = 0; x < COLLISION_MAP_WIDTH; x++) {
      switch (COLLISION_MAP[y][x]) {
        // WALL
        case 'x':
          collision_data->collisions[y][x] = COLLISION_TYPE_WALL;
          break;

        // SUPER DOTS
        case 'o':
          collision_data->collisions[y][x] = COLLISION_TYPE_SUPER_DOTS;
          collision_data->dots_count++;
          break;

        // DOTS
        case '.':
          collision_data->collisions[y][x] = COLLISION_TYPE_DOTS;
          collision_data->dots_count++;
          break;

        // FRUIT
        case 'f':
          collision_data->collisions[y][x] = COLLISION_TYPE_FRUIT;
          collision_data->fruit_spawn = (vec2){x, y};
          break;

        // LEFT DOOR
        case 'l':
          collision_data->collisions[y][x] = COLLISION_TYPE_DOOR_LEFT;
          collision_data->door_left = (vec2){x, y};
          break;

        // RIGHT DOOR
        case 'r':
          collision_data->collisions[y][x] = COLLISION_TYPE_DOOR_RIGHT;
          collision_data->door_right = (vec2){x, y};
          break;

        // GHOST DOOR
        case 'd':
          collision_data->collisions[y][x] = COLLISION_TYPE_GHOST_DOOR;
          break;

        case 'p':
          collision_data->collisions[y][x] = COLLISION_TYPE_NONE;
          collision_data->pacman_spawn = (vec2){x, y};
          break;

        case 'h':
          collision_data->collisions[y][x] = COLLISION_TYPE_HOME;
          collision_data->ghost_home = (vec2){x, y};
          break;

        case '0':
          collision_data->collisions[y][x] = COLLISION_TYPE_NONE;
          collision_data->ghost_spawn[0] = (vec2){x, y};
          break;

        case '1':
          collision_data->collisions[y][x] = COLLISION_TYPE_NONE;
          collision_data->ghost_spawn[1] = (vec2){x, y};
          break;

        case '2':
          collision_data->collisions[y][x] = COLLISION_TYPE_NONE;
          collision_data->ghost_spawn[2] = (vec2){x, y};
          break;

        case '3':
          collision_data->collisions[y][x] = COLLISION_TYPE_NONE;
          collision_data->ghost_spawn[3] = (vec2){x, y};
          break;

        // EMPTY
        case ' ':
        default:
          collision_data->collisions[y][x] = COLLISION_TYPE_NONE;
          break;
      }
    }
  }
}

void collision_destroy() {
  if (!collision_data)
    return;

  free(collision_data);
}

collision_data_t *get_collision_data() { return collision_data; }

b8 is_colliding(vec2 position, collision_type_e type) {
  collision_type_e collision_type = get_collision_type(position);

  return collision_type == type || collision_type == COLLISION_TYPE_MAX;
}

b8 is_colliding_solid(vec2 position) {
  return is_colliding(position, COLLISION_TYPE_WALL) ||
         is_colliding(position, COLLISION_TYPE_GHOST_DOOR);
}

i8 is_colliding_ghost(vec2 position) {
  for (int i = 0; i < GHOST_COLOR_COUNT; i++) {
    vec2 ghost_post = collision_data->ghost_position[i];
    if (vec2_equal(ghost_post, position)) {
      return i;
    }
  }

  return -1;
}

collision_type_e get_collision_type(vec2 position) {
  int x = floor(position.x);
  int y = floor(position.y);

  if (x < 0 || x >= COLLISION_MAP_WIDTH || y < 0 || y >= COLLISION_MAP_HEIGHT) {
    return COLLISION_TYPE_MAX;
  }

  return collision_data->collisions[y][x];
}

void collision_debug_render(vec2 offset_position, vec2 pacman_position) {
#ifndef PRELEASE
  for (int y = 0; y < COLLISION_MAP_HEIGHT; y++) {
    for (int x = 0; x < COLLISION_MAP_WIDTH; x++) {
      vec2 position = (vec2){x * CELL_SIZE - 1 + offset_position.x,
                             y * CELL_SIZE + offset_position.y};

      b8 fill = x == floor(pacman_position.x) && y == floor(pacman_position.y);

      switch (collision_data->collisions[y][x]) {
        case COLLISION_TYPE_WALL:
          render_debug_case(position, (pixel_color_t){255, 0, 255, 255}, fill);
          break;
        case COLLISION_TYPE_SUPER_DOTS:
          render_debug_case(position, (pixel_color_t){0, 255, 0, 255}, fill);
          break;
        case COLLISION_TYPE_DOTS:
          render_debug_case(position, (pixel_color_t){255, 255, 0, 255}, fill);
          break;
        case COLLISION_TYPE_FRUIT:
          render_debug_case(position, (pixel_color_t){255, 128, 0, 255}, fill);
          break;
        case COLLISION_TYPE_GHOST_DOOR:
          break;
        case COLLISION_TYPE_DOOR_LEFT:
        case COLLISION_TYPE_DOOR_RIGHT:
          render_debug_case(position, (pixel_color_t){0, 255, 255, 0}, fill);
          break;
        case COLLISION_TYPE_NONE:
        default:
          break;
      }
    }
  }
#endif
}

void collision_update_type(vec2 position, collision_type_e type) {
  int x = floor(position.x);
  int y = floor(position.y);

  if (x < 0 || x >= COLLISION_MAP_WIDTH || y < 0 || y >= COLLISION_MAP_HEIGHT) {
    return;
  }

  collision_data->collisions[y][x] = type;
}

void collision_remove_dot(vec2 position) {
  int x = floor(position.x);
  int y = floor(position.y);

  if (x < 0 || x >= COLLISION_MAP_WIDTH || y < 0 || y >= COLLISION_MAP_HEIGHT) {
    return;
  }

  collision_type_e type = collision_data->collisions[y][x];
  if (type != COLLISION_TYPE_DOTS) {
    return;
  }

  collision_data->collisions[y][x] = COLLISION_TYPE_NONE;
  collision_data->dots_count--;
}

void collision_remove_super_dot(vec2 position) {
  int x = floor(position.x);
  int y = floor(position.y);

  if (x < 0 || x >= COLLISION_MAP_WIDTH || y < 0 || y >= COLLISION_MAP_HEIGHT) {
    return;
  }

  collision_type_e type = collision_data->collisions[y][x];
  if (type != COLLISION_TYPE_SUPER_DOTS) {
    return;
  }

  collision_data->collisions[y][x] = COLLISION_TYPE_NONE;
  collision_data->dots_count--;
}

u32 collision_get_dots_count() { return collision_data->dots_count; }

b8 is_colliding_door(vec2 position) {
  return is_colliding(position, COLLISION_TYPE_DOOR_LEFT) ||
         is_colliding(position, COLLISION_TYPE_DOOR_RIGHT);
}