#pragma once

#include "application/math.h"

typedef enum {
  TRESHOLD_CHERRY,
  TRESHOLD_STRAWBERRY,
  TRESHOLD_ORANGE,
  TRESHOLD_APPLE,
  TRESHOLD_MELON,
  TRESHOLD_GALAXIAN, 
  TRESHOLD_BELL,
  TRESHOLD_KEY,
} bonuses_treshold_e;

typedef struct {
    u32 value;
    u32 index;
} Threshold;


void bonuses_update(u32 dots_eaten , vec2 start_map_position);