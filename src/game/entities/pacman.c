#include "pacman.h"
#include "../systems/score.h"
#include "SDL_rect.h"
#include "SDL_surface.h"
#include "application/application.h"
#include "application/clock.h"
#include "application/math.h"
#include "application/sprites.h"
#include "defines.h"
#include "game/collision.h"
#include "game/directions.h"
#include "game/entities/ghost.h"
#include "game/entities/pacman.h"
#include "game/systems/audio.h"
#include "game/systems/score.h"
#include <stdio.h>
#include <stdlib.h>

#define INTERPOLATION_FACTOR 0.15f

pacman_t *pacman_create() {
  collision_data_t *collision_data = get_collision_data();
  if (!collision_data)
    return NULL;

  pacman_t *pacman = malloc(sizeof(pacman_t));
  pacman->position = collision_data->pacman_spawn;
  pacman->tile_offset = (vec2){0, 0};
  pacman->is_moving = false;
  pacman->is_super_mode = false;

  pacman->direction = DIRECTION_NONE;
  pacman->next_direction = DIRECTION_NONE;

  pacman->eat_sound1 = audio_create_sound();
  pacman->eat_sound2 = audio_create_sound();
  pacman->super_music = audio_create_music();

  for (int i = 0; i < DIRECTION_COUNT; i++) {
    pacman->sprites[i] = animated_sprite_create(PACMAN_SPRITE[i], 3, 5);
  }

  if (audio_load_sound("./assets/sounds/munch_1.wav", pacman->eat_sound1))
    printf("Failed to load sound: %s\n", Mix_GetError());

  if (audio_load_sound("./assets/sounds/munch_2.wav", pacman->eat_sound2))
    printf("Failed to load sound: %s\n", Mix_GetError());
  if (audio_load_music("./assets/sounds/power_pellet.wav", pacman->super_music))
    printf("Failed to load sound: %s\n", Mix_GetError());
  return pacman;
}

void pacman_destroy(pacman_t *pacman) {
  if (!pacman)
    return;

  audio_destroy_sound(pacman->eat_sound1);
  audio_destroy_sound(pacman->eat_sound2);

  free(pacman);
}

void pacman_update(pacman_t *pacman) {
  if (!pacman)
    return;
  // Update clock for super mode
  if (pacman->is_super_mode) {
    clock_update(&pacman->super_mode_clock);
    if (pacman->super_mode_clock.elapsed > 10) {
      pacman->is_super_mode = false;
      audio_stop_music();
      clock_reset(&pacman->super_mode_clock);
    }
  }

  // Check if the pacman is moving
  if (pacman->is_moving) {
    // Get the raw velocity from the direction
    vec2 raw_velocity = convert_direction_to_vec2(pacman->direction);

    if (fabsf(pacman->tile_offset.x) > 1 || fabsf(pacman->tile_offset.y) > 1) {
      // If the tile offset is greater than 1, reset it
      pacman->position = vec2_add(pacman->position, raw_velocity);

      pacman->tile_offset = (vec2){0, 0};
      pacman->is_moving = false;
      return;
    } else {
      // If it isn't, update the tile offset
      pacman->tile_offset =
          vec2_add(pacman->tile_offset,
                   vec2_multiply(raw_velocity, INTERPOLATION_FACTOR));
    }
    return;
  }

  // Since pacman isn't moving, check if it can move
  vec2 raw_velocity = convert_direction_to_vec2(pacman->next_direction);

  // Check if the next position is an obstacle
  vec2 next_position = vec2_add(pacman->position, raw_velocity);
  if (is_colliding_solid(next_position) && !is_colliding_door(next_position)) {
    // If it is, don't move
    pacman->direction = DIRECTION_NONE;
    return;
  } else {
    // If it isn't, move
    pacman->direction = pacman->next_direction;

    if (pacman->direction == DIRECTION_NONE)
      return;
    pacman->is_moving = true;
  }

  if (is_colliding(pacman->position, COLLISION_TYPE_DOOR_LEFT)) {
    pacman->tile_offset = (vec2){0, 0};
    pacman->position = get_collision_data()->door_right;
    pacman->direction = DIRECTION_LEFT;
    pacman->next_direction = DIRECTION_LEFT;
  } else if (is_colliding(pacman->position, COLLISION_TYPE_DOOR_RIGHT)) {
    pacman->tile_offset = (vec2){0, 0};
    pacman->position = get_collision_data()->door_left;
    pacman->direction = DIRECTION_RIGHT;
    pacman->next_direction = DIRECTION_RIGHT;
  } else if (is_colliding(pacman->position, COLLISION_TYPE_DOTS)) {
    // Remove the dot
    collision_remove_dot(pacman->position);

    // Add score
    score_add(SCORE_PAC_DOT);

    // Add sound
    audio_play_sound(pacman->eat_sound1, false);
    audio_play_sound(pacman->eat_sound2, false);
  } else if (is_colliding(pacman->position, COLLISION_TYPE_SUPER_DOTS)) {
    // Remove the dot
    collision_remove_super_dot(pacman->position);

    // Add score
    score_add(SCORE_POWER_DOT);

    // Add sound
    audio_play_sound(pacman->eat_sound1, false);
    audio_play_sound(pacman->eat_sound2, false);
    pacman->is_super_mode = true;
    clock_start(&pacman->super_mode_clock);
  }
}

void pacman_draw(pacman_t *pacman, vec2 start_map_pos) {
  if (!pacman)
    return;

  vec2 pos_with_offset = vec2_add(pacman->position, pacman->tile_offset);
  vec2 pac_pos = vec2_add(start_map_pos, vec2_multiply(pos_with_offset, 8));
  // Draw the pacman
  render_animated_sprite(get_spritesheet(), pacman->sprites[pacman->direction],
                         pac_pos);

  // Draw a single red pixel
  render_debug_case(pac_pos, (pixel_color_t){255, 0, 0, 255}, false);
}

void pacman_set_direction(pacman_t *pacman, direction_t direction) {
  if (!pacman)
    return;

  pacman->next_direction = direction;
}