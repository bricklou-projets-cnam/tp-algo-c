#include "SDL_timer.h"
#include "application/application.h"
#include "application/arrays/dictionary.h"
#include "application/arrays/linked_list.h"
#include "application/math.h"
#include "defines.h"
#include "game/collision.h"
#include "game/directions.h"
#include "game/entities/ghost.h"
#include "game/entities/pacman.h"
#include "ghosts_logic.h"
#include <math.h>

#define INTERPOLATION_FACTOR 0.15f

linked_list_t* get_path(dict_t* dict, linked_list_node_t* target) {
    linked_list_t* path = linked_list_create();
    dict_node_t* tmp = dictionary_find(dict, target->value);

    while (tmp != NULL && !vec2_equal(tmp->value, INVALID_VECT2)) {
        linked_list_add(path, tmp->key);
        tmp = dictionary_find(dict, tmp->value);

        {
            vec2 pos = vec2_multiply(tmp->value, 8);
            pos = vec2_add(pos, (vec2){21, 21.33});
            render_debug_case(pos, (pixel_color_t){255, 255, 255, 255}, true);
        }
    }

    return path;
}

linked_list_t* bfs(vec2 source, vec2 target) {
    // Create a queue to store the tiles to visit
    linked_list_t* queue = linked_list_create();
    linked_list_add(queue, source);

    dict_t* parents = dictionary_create();
    dictionary_insert(parents, source, INVALID_VECT2);

    while (!linked_list_empty(queue)) {
        linked_list_node_t* cur = linked_list_pop_front(queue);

        if (vec2_equal(cur->value, target)) {
            linked_list_t* final_list =  get_path(parents, cur);
            free(cur);
            linked_list_destroy(queue);
            dictionary_destroy(parents);
            return final_list;
        }

        for (int i = DIRECTION_NONE; i < DIRECTION_COUNT; ++i) {
            vec2 next = vec2_add(cur->value, convert_direction_to_vec2(i));

            if (is_colliding_solid(next))
                continue;

            if (vec2_equal(next, get_collision_data()->door_left))
                next = get_collision_data()->door_right;

            if (vec2_equal(next, get_collision_data()->door_right))
                next = get_collision_data()->door_left;

            if (!dictionary_find(parents, next)) {
                linked_list_add(queue, next);
                dictionary_insert(parents, next, cur->value);
            }
        }

        free(cur);
    }

    dictionary_destroy(parents);
    linked_list_destroy(queue);

    return NULL;
}

direction_t get_direction(vec2 source, vec2 target) {
    vec2 direction = vec2_subtract(target, source);

    if (is_colliding_door(target))
        direction.x = -direction.x;

    if (direction.x > 0)
        return DIRECTION_RIGHT;
    if (direction.x < 0)
        return DIRECTION_LEFT;
    if (direction.y > 0)
        return DIRECTION_DOWN;
    if (direction.y < 0)
        return DIRECTION_UP;

    return DIRECTION_NONE;
}

direction_t find_next_tile(ghost_t* ghost, vec2 target) {
    direction_t direction_to_move = DIRECTION_NONE;

    // Check if the ghost is moving
    if (ghost->is_moving) {
        // If it is, return
        return direction_to_move;
    }

    // Get the path to the target
    linked_list_t* path = bfs(ghost->position, target);

    // Check if the path is empty
    if (path == NULL || linked_list_empty(path)) {
        // If it is, return
        return direction_to_move;
    }

    // Get the next tile
    vec2 next_tile = linked_list_pop(path)->value;

    // Check if the next tile is the same as the current tile
    if (vec2_equal(next_tile, ghost->position)) {
        // If it is, return
        return direction_to_move;
    }

    // Free the path
    linked_list_destroy(path);

    // Set the direction to move if it isn't backward
    if (!is_going_backward(ghost->direction, direction_to_move))
        direction_to_move = get_direction(ghost->position, next_tile);

    // Return the direction to move
    return direction_to_move;
}

void update_position(ghost_t* ghost, direction_t next_direction) {
    // Check if the ghost is moving
    if (ghost->is_moving) {
        // Get the raw velocity from the direction
        vec2 raw_velocity = convert_direction_to_vec2(ghost->direction);

        if (fabs(ghost->tile_offset.x) > 1 || fabs(ghost->tile_offset.y) > 1) {
            // If the tile offset is greater than 1, reset it
            ghost->position = vec2_add(ghost->position, raw_velocity);

            ghost->tile_offset = (vec2){0, 0};
            ghost->is_moving = false;
            return;
        } else {
            // If it isn't, update the tile offset
            ghost->tile_offset =
                vec2_add(ghost->tile_offset,
                        vec2_multiply(raw_velocity, INTERPOLATION_FACTOR * ghost->speed));
        }
        return;
    }

    // Since the ghost isn't moving, check if it can move
    vec2 raw_velocity = convert_direction_to_vec2(next_direction);
    
    // Check if the next position is an obstacle
    vec2 next_position = vec2_add(ghost->position, raw_velocity);
    if (is_colliding_solid(next_position) && !is_colliding_door(ghost->position)) {
        // If it is, don't move
        ghost->direction = DIRECTION_NONE;
        return;
    } else {
        // If it isn't, move
        ghost->direction = next_direction;

        if (ghost->direction == DIRECTION_NONE)
            return;
        ghost->is_moving = true;
    }

    if (is_colliding(ghost->position, COLLISION_TYPE_DOOR_LEFT)) {
        ghost->tile_offset = (vec2){0, 0};
        ghost->position = get_collision_data()->door_right;
        ghost->direction = DIRECTION_LEFT;
    } else if (is_colliding(ghost->position, COLLISION_TYPE_DOOR_RIGHT)) {
        ghost->tile_offset = (vec2){0, 0};
        ghost->position = get_collision_data()->door_left;
        ghost->direction = DIRECTION_RIGHT;
    }
}

void ghost_red_update(ghost_t* ghost, pacman_t* pacman) {
    if (!ghost || !pacman)
        return;

    ghost->target = pacman->position;

    // If the ghost is in scatter mode
    if (ghost->state == GHOST_STATE_SCATTER) {
        return;
    }

    // Then the ghost is in chase mode
    direction_t find = find_next_tile(ghost, pacman->position);

    update_position(ghost, find);
}