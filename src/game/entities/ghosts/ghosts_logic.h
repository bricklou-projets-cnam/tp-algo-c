#pragma once

struct ghost_t;
struct pacman_t;

void ghost_red_update(struct ghost_t* ghost, struct pacman_t* pacman);
void ghost_pink_update(struct ghost_t* ghost, struct pacman_t* pacman);
void ghost_blue_update(struct ghost_t* ghost, struct pacman_t* pacman);
void ghost_orange_update(struct ghost_t* ghost, struct pacman_t* pacman);

void ghost_frightened_update(struct ghost_t* ghost, struct pacman_t* pacman);