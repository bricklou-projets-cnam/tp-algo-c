#include "game/entities/ghost.h"
#include "game/entities/pacman.h"
#include "ghosts_logic.h"

void ghost_orange_update(ghost_t* ghost, pacman_t* pacman) {
    if (!ghost || !pacman)
        return;

    if (ghost->state == GHOST_STATE_SCATTER) {
        return;
    }
}