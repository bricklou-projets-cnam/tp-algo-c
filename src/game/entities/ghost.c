#include "game/entities/ghost.h"
#include "application/application.h"
#include "application/math.h"
#include "application/sprites.h"
#include "defines.h"
#include "game/collision.h"
#include "game/directions.h"
#include "game/entities/ghosts/ghosts_logic.h"
#include "game/entities/pacman.h"
#include "game/systems/score.h"

#include <stdlib.h>

ghost_t *ghosts_init() {
  ghost_t *ghosts = malloc(sizeof(ghost_t) * GHOST_COLOR_COUNT);

  for (int i = 0; i < GHOST_COLOR_COUNT; i++) {
    ghosts[i] = ghost_create(i);
  }

  return ghosts;
}

void ghosts_destroy(ghost_t *ghosts) {
  if (!ghosts)
    return;

  free(ghosts);
}

void ghosts_update(ghost_t *ghosts, pacman_t *pacman) {
  if (!ghosts)
    return;

  for (int i = 0; i < GHOST_COLOR_COUNT; i++) {
    ghost_update(&ghosts[i], pacman);
  }
}

void ghosts_draw(ghost_t *ghosts, pacman_t *pacman, vec2 start_map_position) {
  if (!ghosts)
    return;

  for (int i = 0; i < GHOST_COLOR_COUNT; i++) {
    ghost_draw(&ghosts[i], start_map_position);
  }
}

ghost_t ghost_create(ghost_color_t color) {
  ghost_t ghost = {
      .position = get_collision_data()->ghost_spawn[color],
      .tile_offset = (vec2){0, 0},
      .state = GHOST_STATE_CHASE,
      .color = color,
      .direction = DIRECTION_NONE,
      .is_moving = false,
      .update = NULL,
      .target = (vec2){0, 0,},
      .speed = 1,
  };

  switch (color) {
    case GHOST_COLOR_RED:
      ghost.update = ghost_red_update;
      break;
    case GHOST_COLOR_BLUE:
      ghost.update = ghost_blue_update;
      break;
    case GHOST_COLOR_PINK:
      ghost.update = ghost_pink_update;
      break;
    case GHOST_COLOR_ORANGE:
      ghost.update = ghost_orange_update;
      break;
    default:
      break;
  }

  return ghost;
}

void ghost_destroy(ghost_t *ghost) {
  if (!ghost)
    return;

  free(ghost);
}

void ghost_update(ghost_t *ghost, pacman_t *pacman) {
  if (!ghost)
    return;

  // Update position in collision
  get_collision_data()->ghost_position[ghost->color] = ghost->position;

  if (pacman->is_super_mode) {
    if (ghost->state != GHOST_STATE_FRIGHTENED)
      ghost_set_state(ghost, GHOST_STATE_FRIGHTENED);
  } else {
    if (ghost->state == GHOST_STATE_FRIGHTENED)
      ghost_set_state(ghost, ghost->previous_state);
  }

  if (!ghost->update)
    return;

  if (ghost->state == GHOST_STATE_FRIGHTENED) {
    ghost_frightened_update(ghost, pacman);
  } else {
    ghost->update(ghost, pacman);
  }
}

void ghost_draw(ghost_t *ghost, vec2 start_map_position) {
  if (!ghost)
    return;

  vec2 pos_with_offset = vec2_add(ghost->position, ghost->tile_offset);
  vec2 ghost_pos =
      vec2_add(start_map_position, vec2_multiply(pos_with_offset, 8));
  // Draw the ghost
  if (ghost->state == GHOST_STATE_FRIGHTENED) {
    render_sprite(get_spritesheet(), &GHOST_VULNERABLE_SPRITE, ghost_pos);
  } else {
    render_sprite(get_spritesheet(),
                  &GHOSTS_SPRITES[ghost->color][DIRECTION_NONE][0], ghost_pos);
  }

  // Draw debug case
  render_debug_case(ghost_pos, (pixel_color_t){255, 0, 0}, false);
}

void ghost_set_state(ghost_t *ghost, ghost_state_t state) {
  if (!ghost)
    return;

  ghost->previous_state = ghost->state;
  ghost->state = state;

  if (state == GHOST_STATE_FRIGHTENED) {
    ghost->speed = 0.5;
  } else {
    ghost->speed = 0.9;
  }
}

void ghost_set_position(ghost_t *ghost, vec2 position) {
  if (!ghost)
    return;

  ghost->position = position;
}

b8 ghost_collision_update(ghost_t *ghost, pacman_t *pacman) {
  // Check if colliding a ghost
  i8 color = is_colliding_ghost(pacman->position);

  if (color >= 0) {
    if (pacman->is_super_mode && ghost->state == GHOST_STATE_FRIGHTENED) {
      // Kill the ghost
      ghost_eaten(ghost, pacman);
    } else {
      ghost_kill(ghost, pacman);
      return true;
    }
  }

  return false;
}

void ghost_eaten(ghost_t *ghost, pacman_t *pacman) {
  // Add score
  pacman->eat_ghost_count++;

  switch (pacman->eat_ghost_count) {
    case 1:
      score_add(SCORE_FIRST_GHOST);
      break;
    case 2:
      score_add(SCORE_SECOND_GHOST);
      break;
    case 3:
      score_add(SCORE_THIRD_GHOST);
      break;
    case 4:
      score_add(SCORE_FOURTH_GHOST);
      break;
    default:
      break;
  }

  // Add sound
  audio_play_sound(pacman->eat_sound1, false);

  ghost_set_state(ghost, GHOST_STATE_DEAD);
}

void ghost_kill(ghost_t *ghost, pacman_t *pacman) { pacman->is_dead = true; }