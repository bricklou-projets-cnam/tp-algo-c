#include "bonuses.h"
#include "application/application.h"
#include "game/entities/bonuses.h"
#include "application/sprites.h"
#include "defines.h"
#include "game/collision.h"
#include "application/clock.h"
#include <stdio.h>

#define BONUS_TRESHOLD 70
#define BONUS_DURATION_SECONDS 10

static b8 isBonusVisible = false;
static u32 currentBonusThreshHold = BONUS_TRESHOLD;
static clock bonusStartTime;


const Threshold thresholds[] = {
    {70, TRESHOLD_CHERRY},
    {170, TRESHOLD_STRAWBERRY},
    {270, TRESHOLD_ORANGE},
    {370, TRESHOLD_APPLE},
    {470, TRESHOLD_MELON},
    {570, TRESHOLD_GALAXIAN},
    {670, TRESHOLD_BELL},
    {770, TRESHOLD_KEY}
};

void bonuses_update(u32 dots_eaten , vec2 start_map_pos) {
	vec2 fixed_bonuses_pos = vec2_add(start_map_pos, vec2_multiply(get_collision_data()->fruit_spawn , 8));

	//Check if the score is above the first treshold to determine if the bonus should appear
	if (dots_eaten >= currentBonusThreshHold) {
		// If the bonus is not currently visible, start the timer and render it
        if (!isBonusVisible) {
            isBonusVisible = true;
			clock_start(&bonusStartTime);
			printf("Bonus started at: %f\n", bonusStartTime.start_time);
        }
        render_sprite(get_spritesheet(), &CHERRY_SPRITE, fixed_bonuses_pos);
	}

    //Check if the duration has passed
	clock_update(&bonusStartTime);
	if (isBonusVisible && bonusStartTime.elapsed >= BONUS_DURATION_SECONDS) {
		isBonusVisible = false;
        //currentBonusThreshHold += 100;
		printf("Bonus despawned after: %f\n", bonusStartTime.elapsed);
		//clock_reset();

		// Find the current threshold index
        u32 currentThresholdIndex = 0;
		for (u32 i = 0; i < sizeof(thresholds) / sizeof(thresholds[0]); ++i) {
			if (currentBonusThreshHold == thresholds[i].value) {
				currentThresholdIndex = i;
				break;
			}
		}

		// Calculate the index of the next threshold
        u32 nextThresholdIndex = (currentThresholdIndex + 1) % (sizeof(thresholds) / sizeof(thresholds[0]));

        // Get the next threshold value from the array
        currentBonusThreshHold = thresholds[nextThresholdIndex].value;

	}
}