#include "dots.h"
#include "application/application.h"
#include "application/sprites.h"
#include "defines.h"
#include "game/collision.h"
#include "game/entities/dots.h"

void render_dots(vec2 start_map_position) {
    // Loop on the collision map
    for (int y = 0; y < COLLISION_MAP_HEIGHT; y++) {
        for (int x = 0; x < COLLISION_MAP_WIDTH; x++) {
            collision_type_e collision_type = get_collision_type((vec2){x, y});

            vec2 position = (vec2){x*CELL_SIZE, y*CELL_SIZE};
            position = vec2_add(position, start_map_position);
            // If the current position is a dot
            if (collision_type == COLLISION_TYPE_DOTS){
                render_sprite(get_spritesheet(), &DOT_SPRITE, position);
            } else if (collision_type == COLLISION_TYPE_SUPER_DOTS) {
                render_sprite(get_spritesheet(), &SUPER_DOT_SPRITE, position);
            }/* else if (collision_type == COLLISION_TYPE_FRUIT) {
                render_sprite(get_spritesheet(), &CHERRY_SPRITE, position);
            }*/
        }
    }
}