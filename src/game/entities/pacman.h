#pragma once

#include "application/math.h"
#include "application/sprites.h"
#include "game/directions.h"
#include "game/systems/audio.h"
#include "application/clock.h"

typedef struct pacman_t {
    vec2 position;
    vec2 tile_offset;

    direction_t direction;

    direction_t next_direction;

    b8 is_moving;
    b8 is_super_mode;
    u8 eat_ghost_count;

    b8 is_dead;

    sound_t* eat_sound1;
    sound_t* eat_sound2;

    animated_sprite_t* sprites[DIRECTION_COUNT];
    music_t *super_music;
    clock super_mode_clock;
} pacman_t;

pacman_t* pacman_create();
void pacman_destroy(pacman_t* pacman);

void pacman_update(pacman_t* pacman);
void pacman_draw(pacman_t* pacman, vec2 start_map_position);

void pacman_set_direction(pacman_t* pacman, direction_t direction);