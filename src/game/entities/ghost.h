#pragma once

#include "application/math.h"
#include "application/sprites.h"
#include "game/directions.h"
#include "game/entities/pacman.h"

typedef enum {
  GHOST_STATE_CHASE,
  GHOST_STATE_SCATTER,
  GHOST_STATE_FRIGHTENED,
  GHOST_STATE_DEAD
} ghost_state_t;

typedef enum {
  GHOST_COLOR_RED,
  GHOST_COLOR_PINK,
  GHOST_COLOR_BLUE,
  GHOST_COLOR_ORANGE,

  GHOST_COLOR_COUNT
} ghost_color_t;

typedef struct ghost_t {
  vec2 position;
  vec2 tile_offset;
  direction_t direction;
  f32 speed;

  ghost_state_t state;
  ghost_state_t previous_state;
  ghost_color_t color;

  vec2 target;

  b8 is_moving;

  sprite_t *sprite;

  // Ghost update logic
  void (*update)(struct ghost_t *ghost, struct pacman_t* pacman);
} ghost_t;

// Global methods
ghost_t *ghosts_init();
void ghosts_destroy(ghost_t *ghosts);
void ghosts_update(ghost_t *ghosts, pacman_t *pacman);
void ghosts_draw(ghost_t *ghosts, pacman_t *pacman, vec2 start_map_position);

// Ghost specific methods
ghost_t ghost_create(ghost_color_t color);

void ghost_update(ghost_t *ghost, pacman_t *pacman);
void ghost_draw(ghost_t *ghost, vec2 start_map_position);

void ghost_set_state(ghost_t *ghost, ghost_state_t state);
void ghost_set_position(ghost_t *ghost, vec2 position);

b8 ghost_collision_update(ghost_t *ghost, pacman_t *pacman);
void ghost_eaten(ghost_t *ghost, pacman_t *pacman);
void ghost_kill(ghost_t *ghost, pacman_t *pacman);