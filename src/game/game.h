#pragma once

#include "../defines.h"
#include "../views/view.h"

typedef struct game_t {
    b8 running;
    game_view_t *current_view;
} game_state_t;

b8 game_create();

void game_destroy();

void game_update();

void game_set_current_view(game_view_t *view);