#pragma once

#include "defines.h"
typedef enum {
  SCORE_PAC_DOT = 10,
  SCORE_POWER_DOT = 50,
  SCORE_FIRST_GHOST = 200,
  SCORE_SECOND_GHOST = 400,
  SCORE_THIRD_GHOST = 800,
  SCORE_FOURTH_GHOST = 1600,
  SCORE_CHERRY = 100,
  SCORE_STRAWBERRY = 300,
  SCORE_ORANGE = 500,
  SCORE_APPLE = 700,
  SCORE_MELON = 1000,
  SCORE_GALAXIAN = 2000,
  SCORE_BELL = 3000,
  SCORE_KEY = 5000
} score_points_e;

u32 score_get();
void score_add(score_points_e points);
void score_reset();