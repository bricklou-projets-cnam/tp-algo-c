#include "audio.h"
#include "SDL_mixer.h"
#include "defines.h"
#include "game/systems/audio.h"

#include <stdio.h>

static music_t *playing_music = NULL;
static void (*on_music_finished)() = NULL;

/**
 * @brief Initialise le système audio
 * @return true si une erreur est survenue
 */
b8 audio_init() {
  // Initialisation de SDL Mixer
  if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT,
                    MIX_DEFAULT_CHANNELS, MIX_DEFAULT_CHUNKSIZE) < 0) {
    printf("Erreur lors de l'ouverture du canal audio : %s\n", Mix_GetError());
    // Gérer l'erreur
    return true;
  }

  playing_music = NULL;

  return false;
}

void audio_destroy() {
  if (audio_is_music_playing())
    audio_stop_music();

  on_music_finished = NULL;
  Mix_CloseAudio();
}

music_t *audio_create_music() {
  music_t *music = malloc(sizeof(music_t));
  music->music = NULL;
  return music;
}

b8 audio_load_music(const char *filename, music_t *music) {
  music->music = Mix_LoadMUS(filename);

  if (music->music == NULL) {
    printf("Erreur lors du chargement de la musique : %s\n", Mix_GetError());
    // Gérer l'erreur
    return true;
  }
  return false;
}

void audio_on_music_finished() {
  if (!playing_music)
    return;
  if (on_music_finished)
    on_music_finished();
  playing_music->is_playing = false;
}

void audio_play_music(music_t *music, b8 loop) {
  if (!music || !music->music)
    return;

  if (playing_music)
    audio_stop_music();

  Mix_PlayMusic(music->music, loop ? -1 : 0);
  playing_music = music;
  music->is_playing = true;
  Mix_HookMusicFinished(&audio_on_music_finished);
}

void audio_stop_music() {
  if (!playing_music)
    return;
  Mix_HaltMusic();
  playing_music->is_playing = false;
  playing_music = NULL;
}

void audio_set_on_finished_music(void (*callback)()) {
  on_music_finished = callback;
}

b8 audio_is_music_playing() {
  if (!playing_music)
    return false;
  return playing_music->is_playing;
}

b8 audio_is_current_music(music_t *music) {
  if (!playing_music)
    return false;
  return playing_music == music;
}

void audio_destroy_music(music_t *music) {
  if (!music || !music->music)
    return;

  Mix_FreeMusic(music->music);

  free(music);
}

sound_t *audio_create_sound() {
  sound_t *sound = malloc(sizeof(sound_t));
  sound->sound = NULL;
  return sound;
}

b8 audio_load_sound(const char *filename, sound_t *sound) {
  sound->sound = Mix_LoadWAV(filename);

  if (sound->sound == NULL) {
    printf("Erreur lors du chargement du son : %s\n", Mix_GetError());
    // Gérer l'erreur
    return true;
  }
  return false;
}

void audio_play_sound(sound_t *sound, b8 loop) {
  if (!sound || !sound->sound)
    return;

  Mix_PlayChannel(-1, sound->sound, loop ? -1 : 0);
  sound->is_playing = true;
}

void audio_destroy_sound(sound_t *sound) {
  if (!sound || !sound->sound)
    return;

  Mix_FreeChunk(sound->sound);

  free(sound);
}