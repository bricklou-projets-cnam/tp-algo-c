#include "score.h"
#include "game/systems/score.h"
#include <stdlib.h>

static u32 score = 0;

u32 score_get() {
    return score;
}

void score_add(score_points_e points) {
    score += points;
}

void score_reset() {
    score = 0;
}