#pragma once

#include "defines.h"
#include <SDL2/SDL_mixer.h>

typedef struct music_t {
  Mix_Music *music;
  b8 is_playing;
} music_t;

typedef struct sound_t {
  Mix_Chunk *sound;
  b8 is_playing;
} sound_t;

b8 audio_init();
void audio_destroy();

music_t* audio_create_music();
b8 audio_load_music(const char *filename, music_t* music);
void audio_play_music(music_t* music, b8 loop);
void audio_stop_music();
void audio_destroy_music(music_t* music);
b8 audio_is_music_playing();
b8 audio_is_current_music(music_t* music);
void audio_set_on_finished_music(void (*callback)());

sound_t* audio_create_sound();
b8 audio_load_sound(const char *filename, sound_t* sound);
void audio_play_sound(sound_t* sound, b8 loop);
void audio_destroy_sound(sound_t* sound);