#include "defines.h"
#include "directions.h"
#include "game/directions.h"

vec2 convert_direction_to_vec2(direction_t direction) {
  // Get the raw velocity from the direction
  vec2 raw_velocity;

  switch (direction) {
    case DIRECTION_UP:
      raw_velocity = (vec2){0, -1};
      break;
    case DIRECTION_DOWN:
      raw_velocity = (vec2){0, 1};
      break;
    case DIRECTION_LEFT:
      raw_velocity = (vec2){-1, 0};
      break;
    case DIRECTION_RIGHT:
      raw_velocity = (vec2){1, 0};
      break;
    default:
      raw_velocity = (vec2){0, 0};
      break;
  }

  return raw_velocity;
}

b8 is_going_backward(direction_t direction, direction_t new_direction) {
  switch (direction) {
    case DIRECTION_UP:
      return new_direction == DIRECTION_DOWN;
    case DIRECTION_DOWN:
      return new_direction == DIRECTION_UP;
    case DIRECTION_LEFT:
      return new_direction == DIRECTION_RIGHT;
    case DIRECTION_RIGHT:
      return new_direction == DIRECTION_LEFT;
    default:
      return false;
  }
}