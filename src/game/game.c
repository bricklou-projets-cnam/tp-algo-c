#include "game.h"
#include "../application/application.h"
#include "../views/menu.h"
#include "application/clock.h"
#include "defines.h"
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_keycode.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_video.h>
#include <stdio.h>
#include <stdlib.h>

static game_state_t *state;

b8 game_create() {
  if (!application_create()) {
    printf("Error creating application\n");
    return false;
  }

  state = malloc(sizeof(game_state_t));
  state->running = true;
  state->current_view = menu_create();

  while (state->running) {
    game_update();
  }

  return true;
}

void game_update() {
  // Read inputs
  SDL_Event event;

  clock start_clock;
  clock_start(&start_clock);

  while (SDL_PollEvent(&event)) {
    switch (event.type) {
      case SDL_KEYDOWN:
        if (event.key.keysym.sym == SDLK_ESCAPE) {
          state->running = false;
        }
        break;
      case SDL_QUIT:
        state->running = false;
        break;
      case SDL_WINDOWEVENT:
        if (event.window.event == SDL_WINDOWEVENT_CLOSE) {
          state->running = false;
        }
        break;
      default:
        break;
    }

    if (state->current_view != NULL) {
      b8 skip_current_loop = state->current_view->handle_event(&event);
      if (skip_current_loop)
        return;
    }
  }

  // Clear the window renderer
  renderer_clear();

  // Update the current view
  if (state->current_view != NULL) {
    state->current_view->update();
  }

  // Present the scene
  renderer_present();

  // Calculate the time it took to render the frame
  clock_update(&start_clock);

  // Wait 1/60th of a second
#ifdef PRELEASE
  SDL_Delay(1000.0 / FRAME_RATE - start_clock.elapsed/1000.0);
#else
  SDL_Delay(1000.0 / FRAME_RATE);
#endif
}

void game_destroy() {
  application_destroy();

  if (!state)
    return;

  if (state->current_view) {
    state->current_view->destroy();
    free(state->current_view);
  }

  free(state);
}

void game_set_current_view(game_view_t *view) {
  if (state->current_view) {
    state->current_view->destroy();
    free(state->current_view);
  }

  state->current_view = view;
}