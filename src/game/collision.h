#pragma once

#include "application/math.h"
#include "defines.h"
#include "game/collision.h"
#include "game/entities/ghost.h"

#define COLLISION_MAP_WIDTH 21
#define COLLISION_MAP_HEIGHT 27

static char COLLISION_MAP[COLLISION_MAP_HEIGHT][COLLISION_MAP_WIDTH] = {
    "xxxxxxxxxxxxxxxxxxxxx", //
    "x.........x.........x", //
    "x.xxx.xxx.x.xxx.xxx.x", //
    "xox x.x x.x.x x.x xox", //
    "x.xxx.xxx.x.xxx.xxx.x", //
    "x...................x", //
    "x.xxx.x.xxxxx.x.xxx.x", //
    "x.xxx.x.xxxxx.x.xxx.x", //
    "x.....x...x...x.....x", //
    "xxxxx.xxx x xxx.xxxxx", //
    "    x.x   0   x.x    ", //
    "    x.x xxdxx x.x    ", //
    "xxxxx.x x h x x.xxxxx", //
    "l    .  x123x  .    r", //
    "xxxxx.x xxxxx x.xxxxx", //
    "    x.x   f   x.x    ", //
    "    x.x xxxxx x.x    ", //
    "xxxxx.x xxxxx x.xxxxx", //
    "x.........x.........x", //
    "x.xxx.xxx.x.xxx.xxx.x", //
    "xo..x.....p.....x..ox", //
    "xxx.x.x.xxxxx.x.x.xxx", //
    "xxx.x.x.xxxxx.x.x.xxx", //
    "x.....x...x...x.....x", //
    "x.xxxxxxx.x.xxxxxxx.x", //
    "x...................x", //
    "xxxxxxxxxxxxxxxxxxxxx", //
};

typedef enum collision_type_e {
  // There is nothing
  COLLISION_TYPE_NONE = ' ',

  // Solid objects
  COLLISION_TYPE_WALL = 'x',
  COLLISION_TYPE_GHOST_DOOR = 'd',

  // Collectables
  COLLISION_TYPE_DOTS = '.',
  COLLISION_TYPE_SUPER_DOTS = 'o',
  COLLISION_TYPE_FRUIT = 'f',

  // Entities to spawn
  COLLISION_TYPE_PACMAN = 'p',
  COLLISION_TYPE_GHOST = 'g',
  COLLISION_TYPE_HOME = 'h',

  // Doors
  COLLISION_TYPE_DOOR_LEFT = 'l',
  COLLISION_TYPE_DOOR_RIGHT = 'r',

  // Ghosts spawn
  COLLISION_TYPE_GHOST_SPAWN_0 = '0',
  COLLISION_TYPE_GHOST_SPAWN_1 = '1',
  COLLISION_TYPE_GHOST_SPAWN_2 = '2',
  COLLISION_TYPE_GHOST_SPAWN_3 = '3',

  // Max Value
  COLLISION_TYPE_MAX = 255,
} collision_type_e;

typedef struct collision_data {
  collision_type_e collisions[COLLISION_MAP_HEIGHT][COLLISION_MAP_WIDTH];
  vec2 pacman_spawn;
  vec2 fruit_spawn;
  vec2 ghost_home;

  vec2 door_left;
  vec2 door_right;

  u32 dots_count;

  vec2 ghost_spawn[4];
  vec2 ghost_position[GHOST_COLOR_COUNT];
} collision_data_t;

void collision_init();
void collision_destroy();

b8 is_colliding(vec2 position, collision_type_e type);
b8 is_colliding_solid(vec2 position);
i8 is_colliding_ghost(vec2 position);
b8 is_colliding_door(vec2 position);

void collision_update_type(vec2 position, collision_type_e type);
void collision_remove_dot(vec2 position);
void collision_remove_super_dot(vec2 position);
u32 collision_get_dots_count();

collision_type_e get_collision_type(vec2 position);

collision_data_t *get_collision_data();

void collision_debug_render(vec2 offset_position, vec2 pacman_position);