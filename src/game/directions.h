#pragma once

#include "application/math.h"
typedef enum pacman_direction_t {
    DIRECTION_NONE,
    DIRECTION_UP,
    DIRECTION_DOWN,
    DIRECTION_LEFT,
    DIRECTION_RIGHT,

    DIRECTION_COUNT,
} direction_t;

vec2 convert_direction_to_vec2(direction_t direction);

b8 is_going_backward(direction_t direction, direction_t new_direction);