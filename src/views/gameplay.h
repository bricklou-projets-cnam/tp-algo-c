#pragma once

#include "view.h"
#include <SDL2/SDL_events.h>

game_view_t *gameplay_create();
void gameplay_destroy();

void gameplay_update();

b8 gameplay_handle_event(SDL_Event *event);