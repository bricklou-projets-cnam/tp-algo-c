#include "gameplay.h"
#include "../application/application.h"
#include "../application/math.h"
#include "../application/sprites.h"
#include "application/application.h"
#include "application/math.h"
#include "application/sprites.h"
#include "defines.h"
#include "game/collision.h"
#include "game/directions.h"
#include "game/entities/bonuses.h"
#include "game/entities/dots.h"
#include "game/entities/ghost.h"
#include "game/entities/pacman.h"
#include "game/game.h"
#include "game/systems/audio.h"
#include "game/systems/score.h"
#include "view.h"
#include "views/menu.h"
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_keycode.h>
#include <SDL2/SDL_mixer.h>
#include <stdio.h>

typedef struct gameplay_data_t {
  b8 is_button_hovered;

  pacman_t *pacman;
  direction_t direction;

  ghost_t *ghosts;

  music_t *start_music;
  sound_t *death_sound;

  b8 ready_to_start;
  b8 game_over;
} gameplay_data_t;

static gameplay_data_t *gameplay_data;

void on_start_music_finished() {
  audio_set_on_finished_music(NULL);
  gameplay_data->ready_to_start = true;
}

game_view_t *gameplay_create() {
  // Initialize game object
  gameplay_data = malloc(sizeof(gameplay_data_t));

  game_view_t *view = malloc(sizeof(game_view_t));
  view->destroy = gameplay_destroy;
  view->update = gameplay_update;
  view->handle_event = gameplay_handle_event;
  view->data = malloc(sizeof(gameplay_data_t));

  gameplay_data->direction = DIRECTION_NONE;
  gameplay_data->ready_to_start = false;
  gameplay_data->game_over = false;

  // Initialize collisions
  collision_init();
  // Initialize audio
  audio_init();

  // Initialize pacman
  gameplay_data->pacman = pacman_create();

  // Initialize ghosts
  gameplay_data->ghosts = ghosts_init();

  // Initalize music
  audio_init();

  gameplay_data->start_music = audio_create_music();
  if (audio_load_music("./assets/sounds/game_start.wav",
                       gameplay_data->start_music))
    printf("Failed to load music: %s\n", Mix_GetError());

  gameplay_data->death_sound = audio_create_sound();
  if (audio_load_sound("./assets/sounds/death_1.wav",
                       gameplay_data->death_sound))
    printf("Failed to load sound: %s\n", Mix_GetError());

  audio_set_on_finished_music(on_start_music_finished);
  audio_play_music(gameplay_data->start_music, false);

  return view;
}

void gameplay_destroy() {
  audio_stop_music();
  audio_set_on_finished_music(NULL);
  audio_destroy_music(gameplay_data->start_music);
  audio_destroy_sound(gameplay_data->death_sound);

  ghosts_destroy(gameplay_data->ghosts);
  pacman_destroy(gameplay_data->pacman);
  collision_destroy();
  audio_destroy();

  if (!gameplay_data)
    return;

  free(gameplay_data);
}

void gameplay_update() {
  if (!gameplay_data)
    return;

  // If game over
  if (gameplay_data->game_over) {
    audio_stop_music();

    vec2 position = {SCREEN_WIDTH / 2.0 - GAME_OVER_SPRITE.size.width / 2.0,
                     SCREEN_HEIGHT / 2.0 - GAME_OVER_SPRITE.size.height / 2.0};
    render_sprite(get_spritesheet(), &GAME_OVER_SPRITE, position);
    return;
  }

  // If all the dots are eaten
  if (get_collision_data()->dots_count <= 0) {
    game_set_current_view(menu_create());
  }

  // Draw the game map on the top of the screen
  vec2 start_map_position = {SCREEN_WIDTH / 2.0 - PACMAN_MAP.size.width / 2.0,
                             SCREEN_HEIGHT / 12.0};

  render_sprite(get_spritesheet(), &PACMAN_MAP, start_map_position);
  collision_debug_render(start_map_position, gameplay_data->pacman->position);

  start_map_position.x -= 1;

  if (gameplay_data->ready_to_start) {
    // Bonus Update
    bonuses_update(192 - get_collision_data()->dots_count, start_map_position);

    // Update the ghosts
    ghosts_update(gameplay_data->ghosts, gameplay_data->pacman);

    // Update the pacman
    pacman_update(gameplay_data->pacman);

    // Update ghosts is pacman eat them
    b8 killed =
        ghost_collision_update(gameplay_data->ghosts, gameplay_data->pacman);
    if (killed) {
      gameplay_data->game_over = true;
      audio_play_sound(gameplay_data->death_sound, false);
      return;
    }
  }

  // Play audios
  if (gameplay_data->pacman->is_super_mode) {
    if (!audio_is_current_music(gameplay_data->pacman->super_music)) {
      audio_stop_music();
      audio_play_music(gameplay_data->pacman->super_music, true);
    }
  } else {
    if (audio_is_music_playing() &&
        !audio_is_current_music(gameplay_data->start_music))
      audio_stop_music();
  }

  // Draw the ghosts
  ghosts_draw(gameplay_data->ghosts, gameplay_data->pacman, start_map_position);

  // Draw the dots
  render_dots(start_map_position);

  // Draw the pacman
  pacman_draw(gameplay_data->pacman, start_map_position);

  {
    vec2 pacman_pos = get_collision_data()->pacman_spawn;
    pacman_pos = (vec2){pacman_pos.x * 8 + start_map_position.x,
                        pacman_pos.y * 8 + start_map_position.y};
    render_debug_case(pacman_pos, (pixel_color_t){255, 0, 0, 255}, false);
  }

  // Draw the "ready" message
  if (!gameplay_data->ready_to_start) {
    vec2 ready_position =
        vec2_add(start_map_position,
                 vec2_multiply(get_collision_data()->fruit_spawn, 8));
    ready_position.x -= READY_SPRITE.render_size.width / 2.0 - CELL_SIZE / 2.0;
    ready_position.y -= READY_SPRITE.render_size.height / 2.0 - CELL_SIZE / 2.0;

    render_sprite(get_spritesheet(), &READY_SPRITE, ready_position);
  }

  // Get the game FPS
  u32 fps = application_get_fps();
  render_number(get_spritesheet(), fps,
                (vec2){SCREEN_WIDTH - 8 * 3, SCREEN_HEIGHT - 12});

  // Get the game Score
  render_sprite(
      get_spritesheet(), &HIGH_SCORE_SPRITE,
      (vec2){SCREEN_WIDTH / 2.0 - HIGH_SCORE_SPRITE.render_size.width / 2.0,
             2});
  u32 score = score_get();
  render_number(get_spritesheet(), score,
                (vec2){SCREEN_WIDTH / 2.0 - 8 * 2,
                       2 + 4 + HIGH_SCORE_SPRITE.size.height});
}

b8 gameplay_handle_event(SDL_Event *event) {
  if (!gameplay_data->ready_to_start)
    return false;

  direction_t direction = DIRECTION_NONE;
  switch (event->type) {
    case SDL_KEYDOWN:
      switch (event->key.keysym.sym) {
        case SDLK_UP:
          direction = DIRECTION_UP;
          break;
        case SDLK_DOWN:
          direction = DIRECTION_DOWN;
          break;
        case SDLK_LEFT:
          direction = DIRECTION_LEFT;
          break;
        case SDLK_RIGHT:
          direction = DIRECTION_RIGHT;
          break;
      }

      pacman_set_direction(gameplay_data->pacman, direction);
      break;
    default:
      break;
  }

  return false;
}