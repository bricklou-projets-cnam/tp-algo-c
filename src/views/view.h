#pragma once

#include <SDL2/SDL_events.h>
#include "defines.h"

typedef struct game_view_t {
    void (*destroy)();

    void (*update)();

    b8 (*handle_event)(SDL_Event *event);

    void *data;
} game_view_t;