#pragma once

#include "view.h"
#include <SDL2/SDL_events.h>

game_view_t* menu_create();
void menu_destroy();

void menu_update();

b8 menu_handle_event(SDL_Event *event);