#include "menu.h"
#include "../application/application.h"
#include "../application/math.h"
#include "../application/sprites.h"
#include "../game/game.h"
#include "gameplay.h"
#include "view.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_keycode.h>
#include <SDL2/SDL_rect.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct menu_data_t {
  vec2 BUTTON_POSITION;
  b8 is_button_hovered;
} menu_data_t;

static menu_data_t *menu_data;

void update_button_pos() {
  menu_data->BUTTON_POSITION.x =
      SCREEN_WIDTH / 2.0 - MENU_PLAY_BUTTON_SPRITE.size.width / 2.0;
  menu_data->BUTTON_POSITION.y =
      SCREEN_HEIGHT / 4.0 * 3 - MENU_PLAY_BUTTON_SPRITE.size.height;
}

game_view_t *menu_create() {
  menu_data = malloc(sizeof(menu_data_t));

  game_view_t *view = malloc(sizeof(game_view_t));
  view->destroy = menu_destroy;
  view->update = menu_update;
  view->handle_event = menu_handle_event;
  view->data = malloc(sizeof(menu_data_t));

  update_button_pos();

  return view;
}

void menu_destroy() {
  if (!menu_data)
    return;

  printf("Destroying menu view\n");

  free(menu_data);
}

void menu_update() {
  if (!menu_data)
    return;

  // Draw the game logo
  vec2 position = {SCREEN_WIDTH / 2.0 - LOGO_SPRITE.size.width / 2.0,
                   SCREEN_HEIGHT / 4.0};

  render_sprite(get_spritesheet(), &LOGO_SPRITE, position);

  // Draw the button
  update_button_pos();

  if (menu_data->is_button_hovered) {
    render_sprite(get_spritesheet(), &MENU_PLAY_BUTTON_HOVER_SPRITE,
                  menu_data->BUTTON_POSITION);
  } else {
    render_sprite(get_spritesheet(), &MENU_PLAY_BUTTON_SPRITE,
                  menu_data->BUTTON_POSITION);
  }
}

b8 menu_handle_event(SDL_Event *event) {
  viewport_t *viewport = get_viewport();

  switch (event->type) {
    case SDL_MOUSEMOTION: {
      menu_data->is_button_hovered = false;

      vec2 btn_start = map_viewport_to_screen(menu_data->BUTTON_POSITION);
      vec2 btn_end = vec2_add(btn_start, vec2_multiply(MENU_PLAY_BUTTON_SPRITE.size, viewport->scale));

      // Check if the mouse is hovering over the button
      if (event->motion.x >= btn_start.x && event->motion.x <= btn_end.x &&
          event->motion.y >= btn_start.y && event->motion.y <= btn_end.y) {
        menu_data->is_button_hovered = true;
      }
    } break;
    case SDL_MOUSEBUTTONDOWN:
      if (menu_data->is_button_hovered) {
        printf("Play button pressed\n");
        game_set_current_view(gameplay_create());
      }
    default:
      break;
  }

  return false;
}