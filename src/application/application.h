#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_surface.h>
#include <SDL2/SDL_video.h>

#include "../defines.h"
#include "defines.h"
#include "math.h"
#include "sprites.h"
#include "viewport.h"
#include "clock.h"

typedef struct application_t {
    SDL_Renderer* renderer;
    SDL_Surface* surface;
    SDL_Window* window;

    viewport_t* viewport;

    vec2 window_size;

    clock last_frame_time;
    u64 frame_count;
    u32 fps;
} application_t;

typedef struct pixel_color_t {
    u8 r;
    u8 g;
    u8 b;
    u8 a;
} pixel_color_t;

b8 application_create();

void application_destroy();

void renderer_clear();
void renderer_present();
void render_sprite(SDL_Texture *texture, const sprite_t* sprite, vec2 position);
void render_animated_sprite(SDL_Texture *texture, animated_sprite_t* animated_sprite, vec2 position);
void render_number(SDL_Texture *texture, i32 number, vec2 position);

void render_debug_pixel(vec2 position, pixel_color_t color);
void render_debug_case(vec2 position, pixel_color_t color, b8 fill);

vec2 get_window_size();

viewport_t* get_viewport();
vec2 map_viewport_to_screen(vec2 position);

u32 application_get_fps();
u32 application_get_current_frame();