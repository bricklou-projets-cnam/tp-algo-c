#include "sprites.h"
#include "application/sprites.h"
#include "defines.h"
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_surface.h>
#include <stdio.h>

#ifdef PPLATFORM_WINDOWS
#include <io.h>
#define F_OK 0
#define access _access
#else
#include <unistd.h>
#endif

static SDL_Texture *SPRITESHEET = NULL;

#define NUMBER_SPRITE_COUNT 11
static sprite_t NUMBER_SPRITES[NUMBER_SPRITE_COUNT];

const SDL_Rect SPRITE_NB_START_POS = {4, 256, 7, 7};

b8 load_sprites(const char *path, SDL_Renderer *renderer) {
  if (!path) {
    printf("Error loading sprites: path is null\n");
    return false;
  }

  if (access(path, F_OK) != 0) {
    printf("Error loading sprites: file does not exist\n");
    return false;
  }

  // Load the spritesheet into a surface
  SDL_Surface *surface = SDL_LoadBMP(path);
  // Get rid of the black background
  SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, 0, 0, 0));
  SPRITESHEET = SDL_CreateTextureFromSurface(renderer, surface);
  SDL_FreeSurface(surface);

  if (!SPRITESHEET) {
    printf("Error loading sprites: %s\n", SDL_GetError());
    return false;
  }

  // Load the sprites for the numbers
  for (int i = 0; i < NUMBER_SPRITE_COUNT; i++) {
    NUMBER_SPRITES[i] = (sprite_t) {
      .position = (vec2){.x = SPRITE_NB_START_POS.x + i * 8,
                         .y = SPRITE_NB_START_POS.y},
      .size = (vec2){.x = SPRITE_NB_START_POS.w, .y = SPRITE_NB_START_POS.h},
      .render_size =
          (vec2){.x = SPRITE_NB_START_POS.w, .y = SPRITE_NB_START_POS.h},
    };
  }

  return true;
}

void unload_sprites() {
  if (SPRITESHEET) {
    SDL_DestroyTexture(SPRITESHEET);
    SPRITESHEET = NULL;
  }
}

SDL_Texture *get_spritesheet() { return SPRITESHEET; }

sprite_t* get_number_sprites() {
    return NUMBER_SPRITES;
}

// Animated sprite
animated_sprite_t* animated_sprite_create(const sprite_t* sprite, u32 frame_count, u32 frame_duration) {
  animated_sprite_t* animated_sprite = malloc(sizeof(animated_sprite_t));

  animated_sprite->sprite = sprite;
  animated_sprite->frame_count = frame_count;
  animated_sprite->current_frame = 0;
  
  animated_sprite->frame_duration = frame_duration;
  animated_sprite->current_frame_duration = 0;

  return animated_sprite;
}

void animated_sprite_update(animated_sprite_t *animated_sprite) {
  if (!animated_sprite)
    return;

  animated_sprite->current_frame_duration++;
  if (animated_sprite->current_frame_duration < animated_sprite->frame_duration)
    return;

  animated_sprite->current_frame_duration = 0;

  animated_sprite->current_frame++;
  if (animated_sprite->current_frame >= animated_sprite->frame_count)
    animated_sprite->current_frame = 0;
}

void animated_sprite_reset(animated_sprite_t *animated_sprite) {
  if (!animated_sprite)
    return;

  animated_sprite->current_frame = 0;
}

void animated_sprite_destroy(animated_sprite_t *animated_sprite) {
  if (!animated_sprite)
    return;

  free(animated_sprite);
}

sprite_t* animated_sprite_current(const animated_sprite_t* animated_sprite) {
  if (!animated_sprite)
    return NULL;

  return (sprite_t*)animated_sprite->sprite + animated_sprite->current_frame;
}