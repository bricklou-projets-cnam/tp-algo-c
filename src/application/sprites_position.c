#include "application/sprites.h"
#include "defines.h"
#include "game/directions.h"
#include "sprites.h"

const sprite_t LOGO_SPRITE = {
    .position = {4, 4}, .size = {180, 46}, .render_size = {180, 46}};

const sprite_t MENU_PLAY_BUTTON_SPRITE = {
    .position = {89, 54}, .size = {46, 7}, .render_size = {46, 7}};

const sprite_t MENU_PLAY_BUTTON_HOVER_SPRITE = {
    .position = {89, 63}, .size = {46, 7}, .render_size = {46, 7}};

const sprite_t PACMAN_MAP = {
    .position = {201, 4}, .size = {166, 214}, .render_size = {166, 214}};

const sprite_t PACMAN_SPRITE[DIRECTION_COUNT][3] = {
    {
        {.position = {4, 90},
         .size = {14, 14},
         .render_size = {CELL_SIZE, CELL_SIZE}},
        {.position = {4, 90},
         .size = {14, 14},
         .render_size = {CELL_SIZE, CELL_SIZE}},
        {.position = {4, 90},
         .size = {14, 14},
         .render_size = {CELL_SIZE, CELL_SIZE}},
    },
    {
        {.position = {4, 90},
         .size = {14, 14},
         .render_size = {CELL_SIZE, CELL_SIZE}},
        {.position = {89, 90},
         .size = {14, 14},
         .render_size = {CELL_SIZE, CELL_SIZE}},
        {.position = {106, 90},
         .size = {14, 14},
         .render_size = {CELL_SIZE, CELL_SIZE}},
    },
    {
        {.position = {4, 90},
         .size = {14, 14},
         .render_size = {CELL_SIZE, CELL_SIZE}},
        {.position = {123, 90},
         .size = {14, 14},
         .render_size = {CELL_SIZE, CELL_SIZE}},
        {.position = {140, 90},
         .size = {14, 14},
         .render_size = {CELL_SIZE, CELL_SIZE}},
    },
    {
        {.position = {4, 90},
         .size = {14, 14},
         .render_size = {CELL_SIZE, CELL_SIZE}},
        {.position = {55, 90},
         .size = {14, 14},
         .render_size = {CELL_SIZE, CELL_SIZE}},
        {.position = {72, 90},
         .size = {14, 14},
         .render_size = {CELL_SIZE, CELL_SIZE}},
    },
    {
        {.position = {4, 90},
         .size = {14, 14},
         .render_size = {CELL_SIZE, CELL_SIZE}},
        {.position = {21, 90},
         .size = {14, 14},
         .render_size = {CELL_SIZE, CELL_SIZE}},
        {.position = {38, 90},
         .size = {14, 14},
         .render_size = {CELL_SIZE, CELL_SIZE}},
    },
};

const sprite_t DOT_SPRITE = {.position = {159, 92},
                             .size = {10, 10},
                             .render_size = {CELL_SIZE, CELL_SIZE}};

const sprite_t SUPER_DOT_SPRITE = {.position = {175, 91},
                                   .size = {11, 11},
                                   .render_size = {CELL_SIZE, CELL_SIZE}};

const sprite_t CHERRY_SPRITE = {.position = {123, 273},
                                .size = {12, 12},
                                .render_size = {CELL_SIZE, CELL_SIZE}};

const sprite_t STRAWBERRY_SPRITE = {.position = {140, 273},
                                    .size = {12, 12},
                                    .render_size = {CELL_SIZE, CELL_SIZE}};

const sprite_t ORANGE_SPRITE = {.position = {155, 273},
                                .size = {12, 12},
                                .render_size = {CELL_SIZE, CELL_SIZE}};

const sprite_t APPLE_SPRITE = {.position = {171, 273},
                               .size = {12, 12},
                               .render_size = {CELL_SIZE, CELL_SIZE}};

const sprite_t MELON_SPRITE = {.position = {188, 273},
                               .size = {12, 12},
                               .render_size = {CELL_SIZE, CELL_SIZE}};

const sprite_t GALAXIAN_SPRITE = {.position = {204, 273},
                                  .size = {12, 12},
                                  .render_size = {CELL_SIZE, CELL_SIZE}};

const sprite_t BELL_SPRITE = {.position = {220, 273},
                              .size = {14, 14},
                              .render_size = {CELL_SIZE, CELL_SIZE}};

const sprite_t KEY_SPRITE = {.position = {236, 273},
                                .size = {12, 12},
                                .render_size = {CELL_SIZE, CELL_SIZE}};

const sprite_t GHOSTS_SPRITES[4][DIRECTION_COUNT][2] = {
    {// RED GHOST
     {
         // RED GHOST : IDLE
         {.position = {4, 124},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {4, 124},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     },
     {

         // RED GHOST : MOVING UP
         {.position = {72, 124},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {89, 124},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     },
     {

         // RED GHOST : MOVING DOWN
         {.position = {106, 124},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {123, 124},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     },
     {

         // RED GHOST : MOVING LEFT
         {.position = {38, 124},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {55, 124},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     },
     {

         // RED GHOST : MOVING RIGHT
         {.position = {4, 124},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {21, 124},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     }},
    {// PINK GHOST
     {
         // PINK GHOST : IDLE
         {.position = {4, 142},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {4, 124},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     },
     {

         // PINK GHOST : MOVING UP
         {.position = {72, 142},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {89, 142},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     },
     {

         // PINK GHOST : MOVING DOWN
         {.position = {106, 142},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {123, 142},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     },
     {

         // PINK GHOST : MOVING LEFT
         {.position = {38, 142},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {55, 142},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     },
     {

         // PINK GHOST : MOVING RIGHT
         {.position = {4, 142},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {21, 142},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     }},
    {// CYAN GHOST
     {
         // CYAN GHOST : IDLE
         {.position = {4, 160},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {4, 160},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     },
     {

         // CYAN GHOST : MOVING UP
         {.position = {72, 160},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {89, 160},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     },
     {

         // CYAN GHOST : MOVING DOWN
         {.position = {106, 160},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {123, 160},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     },
     {

         // CYAN GHOST : MOVING LEFT
         {.position = {38, 160},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {55, 160},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     },
     {

         // CYAN GHOST : MOVING RIGHT
         {.position = {4, 160},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {21, 160},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     }},
    {// ORANGE GHOST
     {
         // ORANGE GHOST : IDLE
         {.position = {4, 178},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {4, 178},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     },
     {

         // ORANGE GHOST : MOVING UP
         {.position = {72, 178},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {89, 178},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     },
     {

         // ORANGE GHOST : MOVING DOWN
         {.position = {106, 178},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {123, 178},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     },
     {

         // ORANGE GHOST : MOVING LEFT
         {.position = {38, 178},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {55, 178},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     },
     {

         // ORANGE GHOST : MOVING RIGHT
         {.position = {4, 178},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
         {.position = {21, 178},
          .size = {14, 14},
          .render_size = {CELL_SIZE, CELL_SIZE}},
     }},
};

const sprite_t GHOST_VULNERABLE_SPRITE = {
    .position = {4, 196},
    .size = {14, 14},
    .render_size = {CELL_SIZE, CELL_SIZE}};

const sprite_t HIGH_SCORE_SPRITE = {
    .position = {4, 72}, .size = {79, 7}, .render_size = {79, 7}};

const sprite_t READY_SPRITE = {
    .position = {4, 63},
    .size = {46, 7},
    .render_size = {46, 7},
};

const sprite_t GAME_OVER_SPRITE = {
    .position = {4, 54},
    .size = {78, 7},
    .render_size = {78, 7},
};