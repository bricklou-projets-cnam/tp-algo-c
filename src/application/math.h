#pragma once

#include "../defines.h"

/**
 * @brief A 2-element vector.
 */
typedef union vec2_u {
  f32 elements[2];

  union {
    struct {
      f32 x;
      f32 y;
    };
    struct {
      f32 width;
      f32 height;
    };
  };
} vec2;

// Null vector.
extern const vec2 vec2_zero;
extern const vec2 INVALID_VECT2;

vec2 vec2_add(vec2 a, vec2 b);
vec2 vec2_subtract(vec2 a, vec2 b);
vec2 vec2_multiply(vec2 a, float factor);
vec2 vec2_round(vec2 a);
vec2 vec2_floor(vec2 a);
vec2 vec2_ceil(vec2 a);

b8 vec2_equal(vec2 a, vec2 b);