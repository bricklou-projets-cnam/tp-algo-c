#include "application.h"
#include "application/application.h"
#include "application/clock.h"
#include "application/sprites.h"
#include "defines.h"
#include "math.h"
#include "sprites.h"
#include "viewport.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_hints.h>
#include <SDL2/SDL_rect.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_surface.h>
#include <SDL2/SDL_video.h>
#include <stdio.h>

static application_t *app = NULL;

int on_window_resize(void *userdata, SDL_Event *event);
void update_viewport();

b8 application_create() {
  int rendererFlags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC;
  int windowFlags = SDL_WINDOW_RESIZABLE;

  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    printf("Error initializing SDL: %s\n", SDL_GetError());
    return false;
  }

  app = malloc(sizeof(application_t));
  app->window = NULL;
  app->renderer = NULL;
  app->surface = NULL;
  app->viewport = NULL;

  if (!app) {
    return false;
  }

  app->window = SDL_CreateWindow("Pacman", SDL_WINDOWPOS_UNDEFINED,
                                 SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH,
                                 WINDOW_HEIGHT, windowFlags);
  app->window_size = (vec2){.x = WINDOW_WIDTH, .y = WINDOW_HEIGHT};
  app->viewport = viewport_create((vec2){.x = SCREEN_WIDTH, .y = SCREEN_HEIGHT},
                                  (vec2){.x = 0, .y = 0}, 1.0);

  SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "0");

  app->renderer = SDL_CreateRenderer(app->window, -1, rendererFlags);

  if (!app->renderer) {
    printf("Error creating renderer: %s\n", SDL_GetError());
    return false;
  }

  if (!load_sprites("./assets/pacman_sprites.bmp", app->renderer)) {
    printf("Error loading sprites\n");
    return false;
  }

  app->surface = SDL_GetWindowSurface(app->window);

  // Initialize clock
  clock_init();

  // Initialize FPS system
  clock_start(&app->last_frame_time);
  app->frame_count = 0;
  app->fps = 0;

  // Set the clear color
  SDL_SetRenderDrawColor(app->renderer, 0, 0, 0, 255);

  // Listen the resize event
  SDL_AddEventWatch((SDL_EventFilter)on_window_resize, NULL);
  update_viewport();

  return true;
}

void application_destroy() {
  if (!app) {
    return;
  }

  if (app->surface) {
    SDL_FreeSurface(app->surface);
    app->surface = NULL;
  }
  if (app->renderer) {
    SDL_DestroyRenderer(app->renderer);
    app->renderer = NULL;
  }
  if (app->window) {
    SDL_DestroyWindow(app->window);
    app->window = NULL;
  }
  if (app) {
    free(app);
    app = NULL;
  }

  unload_sprites();
}

void renderer_clear() {
  if (!app->renderer) {
    return;
  }

  SDL_SetRenderDrawColor(app->renderer, 0x00, 0x00, 0x00, 0xFF);
  SDL_RenderClear(app->renderer);

  // Clear render surface
  if (SDL_FillRect(app->surface, NULL, 0)) {
    printf("Failed to clear render surface: %s", SDL_GetError());
  }
}

void renderer_present() {
  if (!app->renderer) {
    return;
  }

  SDL_RenderPresent(app->renderer);

  // Update FPS
  app->frame_count++;
  clock_update(&app->last_frame_time);

  if (app->last_frame_time.elapsed >= 1) {
    app->fps = app->frame_count;
    app->frame_count = 0;
    clock_start(&app->last_frame_time);
  }
}

void render_sprite(SDL_Texture *texture, sprite_t const *sprite,
                   vec2 position) {
  if (!app || !app->renderer)
    return;

  SDL_Rect src_rect = {.x = sprite->position.x,
                       .y = sprite->position.y,
                       .w = sprite->size.width,
                       .h = sprite->size.height};

  // Compute the destination rectangle based on the scale and the offset
  viewport_t *viewport = app->viewport;

  SDL_Rect dest_rect;
  dest_rect.x = viewport->offset.x + position.x * viewport->scale;
  dest_rect.y = viewport->offset.y + position.y * viewport->scale;
  dest_rect.w = sprite->render_size.width * viewport->scale;
  dest_rect.h = sprite->render_size.height * viewport->scale;

  SDL_RenderCopy(app->renderer, texture, &src_rect, &dest_rect);
}

void render_animated_sprite(SDL_Texture *texture, animated_sprite_t* animated_sprite, vec2 position) {
  if (!app || !app->renderer)
    return;
  
  sprite_t* current_sprite = animated_sprite_current(animated_sprite);
  render_sprite(texture, current_sprite, position);

  // Update animated sprite
  animated_sprite_update(animated_sprite);
}

void render_number(SDL_Texture *texture, i32 number, vec2 position) {
  if (!app || !app->renderer)
    return;

  u16 digit_count = 0;
  sprite_t *sprites = get_number_sprites();
  u16 digit_width = sprites[0].render_size.width;

  // If the number is negative, render the minus sign
  if (number < 0) {
    render_sprite(texture, &sprites[0], position);
    position.x += digit_width;
    number = -number;
  }

  if (number == 0) {
    render_sprite(texture, &sprites[0], position);
    return;
  }

  // Compute the number of digits
  {
    int tmp = number;
    while (tmp != 0) {
      tmp /= 10;
      ++digit_count;
    }
  }

  position.x += (digit_count - 1) * digit_width;

  for (int i = digit_count; i > 0; --i) {
    i32 digit = number % 10;
    render_sprite(texture, &sprites[digit], position);
    position.x -= digit_width;
    number /= 10;
  }
}

void render_debug_pixel(vec2 position, pixel_color_t color) {
#ifndef PRELEASE
  if (!app || !app->renderer)
    return;

  // Compute the destination rectangle based on the scale and the offset
  viewport_t *viewport = app->viewport;

  SDL_Rect dest_rect;
  dest_rect.x = viewport->offset.x + position.x * viewport->scale;
  dest_rect.y = viewport->offset.y + position.y * viewport->scale;
  dest_rect.w = viewport->scale;
  dest_rect.h = viewport->scale;

  SDL_SetRenderDrawColor(app->renderer, color.r, color.g, color.b, color.a);
  SDL_RenderFillRect(app->renderer, &dest_rect);
#endif
}

void render_debug_case(vec2 position, pixel_color_t color, b8 fill) {
#ifndef PRELEASE
  if (!app || !app->renderer)
    return;

  // Compute the destination rectangle based on the scale and the offset
  viewport_t *viewport = app->viewport;

  SDL_Rect dest_rect;
  dest_rect.x = viewport->offset.x + position.x * viewport->scale;
  dest_rect.y = viewport->offset.y + position.y * viewport->scale;
  dest_rect.w = viewport->scale * 8;
  dest_rect.h = viewport->scale * 8;

  SDL_SetRenderDrawColor(app->renderer, color.r, color.g, color.b, color.a);
  SDL_RenderDrawRect(app->renderer, &dest_rect);

  if (fill) {
    SDL_RenderFillRect(app->renderer, &dest_rect);
  }
#endif
}

void update_viewport() {
  // Update the viewport while keeping the aspect ratio of the game
  // Create an offset if one of the size is smaller than the window size
  // Set the scale to keep the exact aspect ratio and size
  //      Example:
  //          window size: 800x600 -> viewport size: 800x600 and scale 1
  //          window size: 1600x1200 -> viewport size: 800x600 and scale 2
  //          window size: 400x300 -> viewport size: 800x600 and scale 0.5

  vec2 viewport_size = (vec2){.x = SCREEN_WIDTH, .y = SCREEN_HEIGHT};

  float aspect_ratio = (float)viewport_size.width / viewport_size.height;
  float window_aspect_ratio =
      (float)app->window_size.width / app->window_size.height;
  float scale = 1.0;

  if (aspect_ratio > window_aspect_ratio) {
    viewport_size.width = app->window_size.width;
    viewport_size.height = app->window_size.width / aspect_ratio;

    scale = (float)viewport_size.width / SCREEN_WIDTH;
  } else {
    viewport_size.width = app->window_size.height * aspect_ratio;
    viewport_size.height = app->window_size.height;

    scale = (float)viewport_size.height / SCREEN_HEIGHT;
  }

  // Compute the offset
  vec2 offset = {.x = (app->window_size.width - viewport_size.width) / 2,
                 .y = (app->window_size.height - viewport_size.height) / 2};

  viewport_set_size(app->viewport, viewport_size);
  viewport_set_offset(app->viewport, offset);
  viewport_set_scale(app->viewport, scale);
}

int on_window_resize(void *userdata, SDL_Event *event) {
  if (event->type == SDL_WINDOWEVENT &&
      event->window.event == SDL_WINDOWEVENT_RESIZED) {
    app->surface = SDL_GetWindowSurface(app->window);
    app->window_size.x = event->window.data1;
    app->window_size.y = event->window.data2;

    update_viewport();

    return 0;
  }

  return 1;
}

vec2 get_window_size() { return app->window_size; }

viewport_t *get_viewport() { return app->viewport; }

void viewport_debug(viewport_t *viewport) {
  // Draw the viewport rectangle
  SDL_Rect rect = {.x = viewport->offset.x,
                   .y = viewport->offset.y,
                   .w = viewport->size.x,
                   .h = viewport->size.y};

  SDL_SetRenderDrawColor(app->renderer, 0xFF, 0xFF, 0x00, 0xFF);
  SDL_RenderFillRect(app->renderer, &rect);
}

vec2 map_viewport_to_screen(vec2 position) {
  return (vec2){
      .x = app->viewport->offset.x + position.x * app->viewport->scale,
      .y = app->viewport->offset.y + position.y * app->viewport->scale};
}

u32 application_get_fps() { return app->fps; }

u32 application_get_current_frame() { return app->frame_count; }