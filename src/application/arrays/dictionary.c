#include "dictionary.h"
#include "application/arrays/dictionary.h"
#include "application/math.h"
#include <stdlib.h>

dict_t* dictionary_create() {
    dict_t* dict = malloc(sizeof(dict_t));
    dict->head = NULL;
    dict->size = 0;
    
    return dict;
}

void dictionary_destroy(dict_t* dict) {
    dictionary_clear(dict);
    free(dict);
}

void dictionary_insert(dict_t *dict, vec2 key, vec2 value) {
    dict_node_t* node = malloc(sizeof(dict_node_t));
    node->key = key;
    node->value = value;
    node->next = NULL;
    node->prev = NULL;
    
    // Check if the list is empty
    if (dict->head == NULL) {
        // If it is, set the head and tail to the new node
        dict->head = node;
        dict->tail = node;
    } else {
        // If it isn't, set the next node of the tail to the new node
        node->prev = dict->tail;
        dict->tail->next = node;
        dict->tail = node;
    }
    
    dict->size++;
}

void dictionary_remove(dict_t *dict, vec2 key) {
    dict_node_t* node = dictionary_find(dict, key);
    
    // Check if the node was found
    if (node == NULL) {
        // If it wasn't, return
        return;
    }
    
    // Check if the node is the head
    if (node == dict->head) {
        // If it is, set the head to the next node
        dict->head = node->next;
    }
    
    // Check if the node is the tail
    if (node == dict->tail) {
        // If it is, set the tail to the previous node
        dict->tail = node->prev;
    }
    
    // Check if the node has a previous node
    if (node->prev != NULL) {
        // If it does, set the next node of the previous node to the next node
        node->prev->next = node->next;
    }
    
    // Check if the node has a next node
    if (node->next != NULL) {
        // If it does, set the previous node of the next node to the previous node
        node->next->prev = node->prev;
    }
    
    // Free the node
    free(node);
    
    dict->size--;
}

void dictionary_clear(dict_t* dict) {
    while (dict->head != NULL) {
        dictionary_remove(dict, dict->head->key);
    }
}

dict_node_t* dictionary_find(dict_t* dict, vec2 key) {
    dict_node_t* node = dict->head;
    
    // Iterate through the list
    while (node != NULL) {
        // Check if the current node's key is equal to the key
        if (vec2_equal(node->key, key)) {
            // If it is, return the node
            return node;
        }
        
        // Set the current node to the next node
        node = node->next;
    }
    
    // If the key wasn't found, return NULL
    return NULL;
}

vec2* dictionary_get(dict_t* dict, vec2 key) {
    dict_node_t* node = dictionary_find(dict, key);
    
    // Check if the node was found
    if (node == NULL) {
        // If it wasn't, return NULL
        return NULL;
    }
    
    // If it was, return the value
    return &node->value;
}