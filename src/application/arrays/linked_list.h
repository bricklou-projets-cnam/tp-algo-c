#pragma once

#include "application/math.h"

typedef struct linked_list_node_t {
    vec2 value;
    struct linked_list_node_t* next;
    struct linked_list_node_t* prev;
} linked_list_node_t;

typedef struct linked_list_t {
    linked_list_node_t* head;
    linked_list_node_t* tail;
    linked_list_node_t* current;
    u32 size;
} linked_list_t;

linked_list_t* linked_list_create();
void linked_list_destroy(linked_list_t* list);

linked_list_t* linked_list_add(linked_list_t* list, vec2 value);
linked_list_node_t* linked_list_pop(linked_list_t* list);
linked_list_node_t* linked_list_pop_front(linked_list_t* list);
b8 linked_list_empty(linked_list_t* list);

void linked_list_next(linked_list_t* list);
void linked_list_previous(linked_list_t* list);
void linked_list_reset(linked_list_t* list);

linked_list_node_t* linked_list_find(linked_list_t* list, vec2 value);