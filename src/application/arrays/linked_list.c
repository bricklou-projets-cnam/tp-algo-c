#include "linked_list.h"
#include "application/arrays/linked_list.h"
#include "application/math.h"
#include <stdlib.h>

linked_list_t *linked_list_create() {
  linked_list_t *list = malloc(sizeof(linked_list_t));

  list->head = NULL;
  list->tail = NULL;
  list->current = NULL;
  list->size = 0;

  return list;
}

void linked_list_destroy(linked_list_t *list) {
  while (list->head != NULL) {
    linked_list_pop(list);
  }

  free(list);
}

linked_list_t* linked_list_add(linked_list_t* list, vec2 value) {
    linked_list_node_t* node = malloc(sizeof(linked_list_node_t));
    node->value = value;
    node->next = NULL;
    node->prev = NULL;
    
    // Check if the list is empty
    if (list->head == NULL) {
        // If it is, set the head and tail to the new node
        list->head = node;
        list->tail = node;
        // Set the current node to the new node
        list->current = node;
    } else {
        // If it isn't, set the next node of the tail to the new node
        node->prev = list->tail;
        list->tail->next = node;
        list->tail = node;
    }
    
    list->size++;
    
    return list;
}

linked_list_node_t* linked_list_pop(linked_list_t* list) {
    // Check if the list is empty
    if (list->head == NULL) {
        // If it is, return
        return NULL;
    }

    // Check if the list has only one element
    if (list->head == list->tail){
        // If it is, free the head and set it to NULL
        linked_list_node_t* node = list->head;
        list->head = NULL;
        list->tail = NULL;
        list->current = NULL;
        list->size--;

        node->prev = NULL;
        node->next = NULL;
        return node;
    } else {
        // If it isn't, get the previous node
        linked_list_node_t* node = list->tail;
        linked_list_node_t* previous_node = node->prev;

        // Free the tail and set it to the previous node
        list->tail = previous_node;
        list->tail->next = NULL;
        list->size--;

        node->next = NULL;
        node->prev = NULL;
        return node;
    }
}

linked_list_node_t* linked_list_pop_front(linked_list_t* list) {
    // Check if the list is empty
    if (list->head == NULL) {
        // If it is, return
        return NULL;
    }

    // Check if the list has only one element
    if (list->head == list->tail){
        // If it is, free the head and set it to NULL
        linked_list_node_t* node = list->head;
        list->head = NULL;
        list->tail = NULL;
        list->current = NULL;
        list->size--;

        node->prev = NULL;
        node->next = NULL;
        return node;
    } else {
        // If it isn't, get the next node
        linked_list_node_t* node = list->head;
        linked_list_node_t* next_node = node->next;

        // Free the head and set it to the next node
        list->head = next_node;
        list->head->prev = NULL;
        list->size--;

        node->next = NULL;
        node->prev = NULL;
        return node;
    }
}

void linked_list_next(linked_list_t *list) {
    if (list->current == NULL) {
        return;
    }
    
    list->current = list->current->next;
}

void linked_list_previous(linked_list_t *list) {
    if (list->current == NULL) {
        return;
    }
    
    list->current = list->current->prev;
}

void linked_list_reset(linked_list_t *list) {
    list->current = list->head;
}

linked_list_node_t* linked_list_find(linked_list_t *list, vec2 value) {
    linked_list_node_t* node = list->head;
    
    while (node != NULL) {
        if (vec2_equal(node->value, value)) {
            return node;
        }
        
        node = node->next;
    }
    
    return NULL;
}

b8 linked_list_empty(linked_list_t *list) {
    return list->size == 0 || list->head == NULL;
}