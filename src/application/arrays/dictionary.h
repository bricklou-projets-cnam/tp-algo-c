#pragma once

#include "application/arrays/dictionary.h"
#include "application/math.h"

typedef struct dict_node_t {
    vec2 key;
    vec2 value;
    struct dict_node_t *next;
    struct dict_node_t *prev;
} dict_node_t;

typedef struct dict_t {
    struct dict_node_t* head;
    struct dict_node_t* tail;
    u32 size;
} dict_t;

dict_t* dictionary_create();
void dictionary_destroy(dict_t* dict);

void dictionary_insert(dict_t* dict, vec2 key, vec2 value);
void dictionary_remove(dict_t* dict, vec2 key);
void dictionary_clear(dict_t* dict);

dict_node_t* dictionary_find(dict_t* dict, vec2 key);
vec2* dictionary_get(dict_t* dict, vec2 key);