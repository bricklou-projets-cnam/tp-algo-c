#pragma once

#include "application/sprites.h"
#include "defines.h"
#include "game/directions.h"
#include "math.h"
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_surface.h>

typedef struct sprite_t {
  vec2 position;
  vec2 size;
  vec2 render_size;
} sprite_t;

typedef struct animated_sprite_t {
  const sprite_t* sprite;
  u32 frame_count;

  u32 current_frame;

  u32 frame_duration;
  u32 current_frame_duration;
} animated_sprite_t;

extern const sprite_t LOGO_SPRITE;
extern const sprite_t MENU_PLAY_BUTTON_SPRITE;
extern const sprite_t MENU_PLAY_BUTTON_HOVER_SPRITE;
extern const sprite_t PACMAN_MAP;
extern const sprite_t PACMAN_SPRITE[DIRECTION_COUNT][3];
extern const sprite_t DOT_SPRITE;
extern const sprite_t SUPER_DOT_SPRITE;
extern const sprite_t CHERRY_SPRITE;
extern const sprite_t STRAWBERRY_SPRITE;
extern const sprite_t ORANGE_SPRITE;
extern const sprite_t APPLE_SPRITE;
extern const sprite_t MELON_SPRITE;
extern const sprite_t GALAXIAN_SPRITE;
extern const sprite_t BELL_SPRITE;
extern const sprite_t KEY_SPRITE;
extern const sprite_t GHOSTS_SPRITES[4][DIRECTION_COUNT][2];
extern const sprite_t GHOST_VULNERABLE_SPRITE;
extern const sprite_t HIGH_SCORE_SPRITE;
extern const sprite_t READY_SPRITE;
extern const sprite_t GAME_OVER_SPRITE;

// Sprites
b8 load_sprites(const char *path, SDL_Renderer *renderer);
void unload_sprites();
SDL_Texture *get_spritesheet();

sprite_t *get_number_sprites();

// Animated Sprites
animated_sprite_t* animated_sprite_create(const sprite_t* sprite, u32 frame_count, u32 frame_duration);
void animated_sprite_update(animated_sprite_t *animated_sprite);
void animated_sprite_reset(animated_sprite_t *animated_sprite);
void animated_sprite_destroy(animated_sprite_t *animated_sprite);
sprite_t* animated_sprite_current(const animated_sprite_t* animated_sprite);