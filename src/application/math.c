#include "math.h"
#include "application/math.h"
#include <math.h>

// NUll vector
const vec2 vec2_zero = {0, 0};
const vec2 INVALID_VECT2 = {INFINITY, INFINITY};

vec2 vec2_add(vec2 a, vec2 b) {
    return (vec2){.x = a.x + b.x, .y = a.y + b.y};
}

vec2 vec2_subtract(vec2 a, vec2 b) {
    return (vec2){.x = a.x - b.x, .y = a.y - b.y};
}

vec2 vec2_multiply(vec2 a, float factor) {
    return (vec2){.x = a.x * factor, .y = a.y * factor};
}

vec2 vec2_round(vec2 a) {
    return (vec2){.x = round(a.x), .y = round(a.y)};
}

vec2 vec2_floor(vec2 a) {
    return (vec2){.x = floor(a.x), .y = floor(a.y)};
}

vec2 vec2_ceil(vec2 a) {
    return (vec2){.x = ceil(a.x), .y = ceil(a.y)};
}

b8 vec2_equal(vec2 a, vec2 b) {
    return a.x == b.x && a.y == b.y;
}