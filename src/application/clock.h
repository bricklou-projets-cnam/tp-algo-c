#pragma once
#include "defines.h"

/**
 * @brief Represents a basic clock, which can be used to track time
 * deltas in the system.
 */
typedef struct clock {
  /** @brief The start time of the clock. If never started, this is 0. */
  f64 start_time;
  /** @brief The amount of time in seconds that have elapsed since this
   * clock was started. Only accurate after a call to clock_update.
   */
  f64 elapsed;
} clock;

void clock_init();

/**
 * @brief Updates the provided clock. Should be called just before checking
 * elapsed time. Has no effect on non-started clocks.
 * @param clock A pointer to the clock to be updated.
 */
void clock_update(clock *clock);

/**
 * @brief Starts the provided clock. Resets elapsed time.
 * @param clocl A pointer to the clock to be started.
 */
void clock_start(clock *clock);

/**
 * @brief Stops the provided clock. Does not reset elapsed time.
 * @param clock A pointer to the clock to be stopped.
 */
void clock_stop(clock *clock);

/**
 * @brief Resets the provided clock. Does not stop the clock.
 * @param clock A pointer to the clock to be reset.
 */
void clock_reset(clock *clock);

#ifdef PPLATFORM_WINDOWS
f64 get_clock_frequency();
#endif