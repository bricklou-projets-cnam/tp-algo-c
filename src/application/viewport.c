#include "viewport.h"
#include <SDL2/SDL_rect.h>

viewport_t* viewport_create(vec2 size, vec2 offset, f32 scale) {
    viewport_t* viewport = malloc(sizeof(viewport_t));
    viewport->size = size;
    viewport->offset = offset;

    return viewport;
}

void viewport_set_size(viewport_t* viewport, vec2 size) {
    viewport->size = size;
}

void viewport_set_offset(viewport_t* viewport, vec2 offset) {
    viewport->offset = offset;
}

void viewport_set_scale(viewport_t *viewport, f32 scale) {
    viewport->scale = scale;
}