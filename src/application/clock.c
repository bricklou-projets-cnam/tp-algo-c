#include "clock.h"

#include "defines.h"
#include "clock_abs_time.h"

#ifdef PPLATFORM_WINDOWS
#include <windows.h>

static f64 clock_frequency;
static LARGE_INTEGER start_time;

f64 get_clock_frequency() { return clock_frequency; }

#endif

void clock_init() {
#ifdef PPLATFORM_WINDOWS
  // Clock setup
  LARGE_INTEGER frequency;
  QueryPerformanceFrequency(&frequency);
  clock_frequency = 1.0 / (f64)frequency.QuadPart;

  QueryPerformanceCounter(&start_time);
#endif
}

void clock_update(clock *clock) {
  if (clock->start_time != 0) {
    clock->elapsed = get_absolute_time() - clock->start_time;
  }
}

void clock_start(clock *clock) {
  clock->start_time = get_absolute_time();
  clock->elapsed = 0;
}

void clock_stop(clock *clock) { clock->start_time = 0; }

void clock_reset(clock *clock) {
  clock->start_time = 0;
  clock->elapsed = 0;
}
