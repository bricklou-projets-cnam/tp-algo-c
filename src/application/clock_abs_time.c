#include "clock_abs_time.h"

#ifdef PPLATFORM_LINUX
#include <time.h> // nanosleep
#include <unistd.h> // usleep
#endif

#ifdef PPLATFORM_WINDOWS
#include "application/clock.h"
#include <windows.h>
#endif

f64 get_absolute_time() {
#ifdef PPLATFORM_WINDOWS

  LARGE_INTEGER now_time;
  QueryPerformanceCounter(&now_time);
  return (f64)now_time.QuadPart * get_clock_frequency();

#else
  struct timespec now;
  clock_gettime(CLOCK_MONOTONIC_RAW, &now);
  return now.tv_sec + now.tv_nsec * 0.000000001;
#endif
}