#pragma once

#include "math.h"

typedef struct viewport_t {
    vec2 size;
    vec2 offset;
    f32 scale;
} viewport_t;

viewport_t* viewport_create(vec2 size, vec2 offset, f32 scale);

void viewport_set_size(viewport_t* viewport, vec2 size);
void viewport_set_offset(viewport_t* viewport, vec2 offset);
void viewport_set_scale(viewport_t* viewport, f32 scale);

void viewport_destroy(viewport_t* viewport);

void viewport_debug(viewport_t* viewport);