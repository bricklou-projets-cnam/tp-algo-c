add_rules("plugin.vsxmake.autoupdate")
add_rules("plugin.compile_commands.autoupdate", {outputdir = ".vscode"})

add_rules("mode.debug", "mode.release")

add_requires("libsdl", "libsdl_image", "libsdl_mixer")
add_links("SDL2_mixer")

target("pacman")
    set_kind("binary")
    add_files("src/**.c")
    add_includedirs("src")
    set_default(true)

    set_languages("c17")

    add_packages("libsdl", "libsdl_image", "libsdl_mixer")

    after_build(function (target)
        os.cp("assets", target:targetdir())
    end)

    if is_mode("release") then
        add_defines("PRELEASE")
    end